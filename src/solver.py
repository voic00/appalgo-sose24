import math
import sys
from pulp import *
from collections import defaultdict, deque

# Count the number of crossings from the crossing variables
def count_crossings_via_variables(c_vars):
    crossings = 0
    for c_var in c_vars.values():
        if c_var.varValue == 1:
            crossings += 1
    return crossings

# Prioritize cycles based on their length (example: shorter cycles first)
def prioritize_cycles(cycles):
    cycles.sort(key=len, reverse=False)
    return cycles

# Detect cycles in the graph using depth-first search (DFS)
def detect_cycle(graph, node, visited, rec_stack, path, cycles):
    visited[node] = True
    rec_stack[node] = True
    path.append(node)

    for neighbor in graph[node]:
        if not visited[neighbor]:
            detect_cycle(graph, neighbor, visited, rec_stack, path, cycles)
        elif rec_stack[neighbor]:
            cycle_start = path.index(neighbor)
            cycles.append(path[cycle_start:].copy())

    rec_stack[node] = False
    path.pop()

# Find all unique cycles in the graph
def find_all_cycles(graph, nodes):
    visited = {node: False for node in nodes}
    rec_stack = {node: False for node in nodes}
    path = []
    cycles = []

    for node in nodes:
        if not visited[node]:
            detect_cycle(graph, node, visited, rec_stack, path, cycles)

    # Remove duplicate cycles
    unique_cycles = []
    seen = set()
    for cycle in cycles:
        cycle_tuple = tuple(sorted(cycle))
        if cycle_tuple not in seen:
            seen.add(cycle_tuple)
            unique_cycles.append(cycle)

    return unique_cycles

# Add constraints to the linear programming problem based on the detected cycles
def add_cycle_constraints(prob, y, cycles, added_constraints):
    for cycle in cycles:
        cycle = list(dict.fromkeys(cycle))
        for i in range(len(cycle)):
            for j in range(i + 1, len(cycle)):
                constraint_1 = ((cycle[i], cycle[j]), (cycle[j], cycle[i]))
                if constraint_1 not in added_constraints:
                    # One of the two must be true
                    prob += y[(cycle[i], cycle[j])] + y[(cycle[j], cycle[i])] == 1
                    added_constraints.add(constraint_1)
                    
                for k in range(j + 1, len(cycle)):
                    if (cycle[i], cycle[j]) in y and (cycle[j], cycle[k]) in y and (cycle[i], cycle[k]) in y:
                        constraint_2 = ((cycle[i], cycle[j]), (cycle[j], cycle[k]), (cycle[i], cycle[k]))
                        if constraint_2 not in added_constraints:
                            # i < j and j < k -> i < k (transitivity constraint)
                            prob += 0 <= y[(cycle[i], cycle[j])] + y[(cycle[j], cycle[k])] <= 1 + y[(cycle[i], cycle[k])]
                            added_constraints.add(constraint_2)

# Solve the bipartite minimization problem
def solve_bipartite_minimization(input_lines):
    edges = []
    for line in input_lines:
        if line.startswith('c'):
            continue
        elif line.startswith('p'):
            parts = line.split()
            n0 = int(parts[2])
            n1 = int(parts[3])
        else:
            x, y = map(int, line.split())
            edges.append((x, y))
        logging.info(f"{len(edges)} edges loaded.")

    # Define the linear programming problem
    prob = LpProblem("Minimize_Crossings", LpMinimize)
    
    # Define binary variables for the edges
    y = {(i, j): LpVariable(f"y_{i}_{j}", 0, 1, cat='Binary') for i in range(n0 + 1, n0 + n1 + 1) for j in range(n0 + 1, n0 + n1 + 1) if i != j}
    
    # Define binary variables for crossings
    c = {(i, j, k, l): LpVariable(f"c_{i}_{j}_{k}_{l}", 0, 1, cat='Binary') for (i, j) in edges for (k, l) in edges}

    # Objective function: minimize the sum of crossings
    prob += lpSum(c.values())

    # Add constraints to the problem
    for (i, j) in edges:
        for (k, l) in edges:
            if k > i:
                if j > l:
                    prob += c[(i, j, k, l)] == y[(l, j)]
                elif l > j:
                    prob += c[(i, j, k, l)] == 1 - y[(j, l)]

    added_constraints = set()
    # The maximum cycle length at which the elimination of all cycles begins
    max_cycle_length = 9

    while True:
        prob.solve(PULP_CBC_CMD(msg=0))
        if prob.status != LpStatusOptimal:
            break

        # Build the graph based on the current solution
        graph = defaultdict(list)
        in_degree = defaultdict(int)
        nodes = range(n0 + 1, n0 + n1 + 1)
        for i in nodes:
            for j in nodes:
                if i != j:
                    y_ij = y.get((i, j))
                    if y_ij is not None:
                        if y_ij.varValue == 1:
                            graph[i].append(j)
                            in_degree[j] += 1
                        elif y_ij.varValue == 0:
                            graph[j].append(i)
                            in_degree[i] += 1

        # Detect all cycles in the graph
        all_cycles = find_all_cycles(graph, nodes)
        
        if not all_cycles:
            break

        # Prioritize the cycles by their length
        prioritized_cycles = prioritize_cycles(all_cycles)

        # Check the length of the longest cycle and adjust k accordingly
        if len(prioritized_cycles[-1]) > max_cycle_length:
            k = min(int(math.floor(len(prioritized_cycles)/4)), len(prioritized_cycles))
        else:
            k = len(prioritized_cycles)
        
        # Take only the first k cycles from the prioritized list
        cycles_to_process = prioritized_cycles[:k]
        add_cycle_constraints(prob, y, cycles_to_process, added_constraints)

    if prob.status == LpStatusOptimal:
        # Topologically sort the nodes based on the in-degree
        zero_in_degree_queue = deque([i for i in nodes if in_degree[i] == 0])
        sorted_b = []
        while zero_in_degree_queue:
            node = zero_in_degree_queue.popleft()
            sorted_b.append(node)
            for neighbor in graph[node]:
                in_degree[neighbor] -= 1
                if in_degree[neighbor] == 0:
                    zero_in_degree_queue.append(neighbor)

        return sorted_b
    else:
        return None

# Main function to read input and solve the problem
def main():
    input_lines = sys.stdin.read().strip().split('\n')
    result = solve_bipartite_minimization(input_lines)
    
    if result:
        print("\n".join(map(str, result)))
    else:
        print("No result")
        
if __name__ == "__main__":
    main()
